# Docker Compose
## NextJS
### Requirements
- Node >= 14

### How to run
First thing first : start docker desktop or docker hub.

Go on your shell and paste :
```sh
docker-compose up --build -d
```

Then open [http://localhost:3000](http://localhost:3000) in your browser and TADAM !! &#x2728;

Modifying the `pages/index.js` file should automatically update and reload the application.

![image](capture2.png)