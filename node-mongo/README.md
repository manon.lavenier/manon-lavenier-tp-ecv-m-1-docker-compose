# Docker Compose
## NodeJS + MongoDB
### Requirements
- Node >= 14
- MongoDB: an available MongoDB server running on port `27017` and host `mongo` with the root username as `root` and the root password as `verysecret`.

### How to run
Install dependencies with NPM
```sh
npm install
```

Run the application in development mode
```sh
npm run dev
```
### Run with docker

You first need to build all services then launching them with :
```sh
docker compose up --build -d
```

Then open [http://localhost:3000](http://localhost:3000) in your browser and TADAM !! &#x2728;

Modifying the `views/home/mustache` file should automatically update and reload the application.

![image](capture3.png)